#!/usr/bin/perl -w

#   copycheck - find copyright and license sections in source code
#   Copyright (C) 2005 Christoph Berg <myon@debian.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#   perl part - recurses into directories and greps files for keywords

# 2005-11-07: initial version in sh
# 2005-11-28: rewrite in perl

use strict;
use File::Find;
use Digest::MD5;

my $version = "v0.1";

## set up our BFRE

# \b<word>\w*
my $keyprefixes = <<EOL;
author
commercial
confidential
copy(?:right|left)
liability
l?gpl
licen[sc]e
maintain
merchantability
patent
prohibit
proprietary
(?:re)?distribut
release
revision
rights
EOL
$keyprefixes = join '|', map { "\\b$_\\w*"; } split /\s+/, $keyprefixes;
# \b<string>
my $modal = '(?:do |does )?(?:may|must|shall|should|want)';
my $keystrings = "$modal not|you $modal|made by|as is";
# \b<string>
my $symbols = '\(c\)|(?:https?|ftp)://[^ ]*|www[^ ]*|\w[^ ]+@[^ ]+\.[^ ]+\w';
#my $copyre = "(?:$keyprefixes)|\\b(?:$keystrings)|\\b(?:$symbols)";
my $copyre = "(?:$keyprefixes)|(?:$symbols)";

## grep NIH

my $context_pre = 2;
my $context_post = 3;

sub tobold
{
    my $str = shift;
    $str =~ s/(.)/$1\010$1/g;
    return $str;
}

my %seen_chunk;
my $seen_file = "";
sub print_chunk
{
    my ($file, $context, $context_m) = @_;
    my ($cl, $cml) = (scalar @$context, scalar @$context_m);
    my ($key, $out);
    foreach (@$context) {
	my $m = shift @$context_m;
	$key .= $_ if $m;
	s/<[a-z][^>]*>//ig; # strip html
	$out .= $_ unless /^\s*$/; # don't output empty lines
    }
    if ($seen_chunk{$key}) {
	#print "Skipping seen chunk ($cl/$cml)\n";
	return;
    }
    print "$file:" unless $file eq $seen_file;
    print "\n$out";
    $seen_file = $file;
    $seen_chunk{$key} = 1;
}

sub grep_file
{
    my $file = shift;
    open F, $file or warn "$file: $!", return 0;
    my (@context, @context_m);
    my ($match, $post, $line, $ascii, $length) = (0, 0, 0, 0, 0);
    while (<F>) {
	$line++;
	$_ = substr($_, 0, 200);
	if (s/($copyre)/tobold($1)/egio) {
	    $match = 1;
	    $post = 0;
	    push @context_m, 1;
	} else {
	    $post++ if $match;
	    push @context_m, 0;
	}
	push @context, $_;
	if ($line <= 5) {
	    $ascii++ foreach /[[:print:]]/g;
	    $length += length($_);
	}
	if ($line == 5 and $ascii / $length < .6) {
	    print "$file: binary\n";
	    return;
	}
	if (not $match and @context_m > $context_pre) {
	    shift @context;
	    shift @context_m;
	}
	if ($post == $context_post + $context_pre + 1) {
	    splice @context, -$context_pre - 1, 1;
	    my @c = splice @context, -$context_pre; # save last (if lookahead overlaps next chunk)
	    print_chunk($file, \@context, \@context_m);
	    @context = @c;
	    @context_m = (); push @context_m, 0 foreach @context;
	    $match = $post = 0;
	    #print "\n";
	}
    }
    if ($match) {
	print_chunk($file, \@context, \@context_m);
    }
}

## BTDT

my $prefix = $0;
$prefix =~ s!/+[^/]*$!!;

my @common_licenses = map { /^\// ? $_ : "$prefix/$_"; } # qw// does not interpolate. suckers.
qw(
    /usr/share/common-licenses/Artistic
    /usr/share/common-licenses/GPL-2
    /usr/share/common-licenses/LGPL-2
    /usr/share/common-licenses/LGPL-2.1
    licenses/INSTALL-automake1.4
    licenses/INSTALL-automake1.7
    licenses/fdl.tex
    licenses/openssl
);

my %known_licenses = (
    'f921793d03cc6d63ec4b15e9be8fd3f8' => 'Artistic',

    '0636e73ff0215e8d672dc4c32c317bb3' => 'GPL v2',
    '14bc6ee8b2e2b409be599212867d126e' => 'GPL v2',
    '393a5ca445f6965873eca0259a17f833' => 'GPL v2',
    '4325afd396febcb659c36b49533135d4' => 'GPL v2',
    '865985546318ca749fff06785525f95b' => 'GPL v2',
    '8ca43cbc842c2336e835926c2166c28b' => 'GPL v2',
    '94d55d512a9ba36caa9b7df079bae19f' => 'GPL v2',
    '9ad5a0145d032753a64e789714c47479' => 'GPL v2 (xml)',
    'c492e2d6d32ec5c1aad0e0609a141ce9' => 'GPL v2',
    'c93c0550bd3173f4504b2cbd8991e50b' => 'GPL v2',
    'cbbd794e2a0a289b9dfcc9f513d1996e' => 'GPL v2',
    'd368405f1c604912f51ba286eed9944a' => 'GPL v2',
    'ea5bed2f60d357618ca161ad539f7c0a' => 'GPL v2',
    'eb723b61539feef013de476e68b5c50a' => 'GPL v2',
    'fdafc691aa5fb7f8e2a9e9521fef771b' => 'GPL v2',

    '55ca817ccb7d5b5b66355690e9abc605' => 'LGPL v2.0',
    'c46bda00ffbb0ba1dac22f8d087f54d9' => 'LGPL v2.0',

    '2d5025d4aa3495befef8f17206a5b0a1' => 'LGPL v2.1',
    '651bc9dd34654ee6a7811885abd4f54d' => 'LGPL v2.1',
    '7fbc338309ac38fefcd64b04bb903e34' => 'LGPL v2.1',
    'a6f89e2100d9b6cdffcea4f398e37343' => 'LGPL v2.1',
    'af499283a192ff12caea399cd6d9d014' => 'LGPL v2.1',
    'bbb461211a33b134d42ed5ee802b37ff' => 'LGPL v2.1',
    'd8045f3b8f929c1cb29a1e3fd737b499' => 'LGPL v2.1',
    'fad9b3332be894bab9bc501572864b29' => 'LGPL v2.1',

    '3e7e40ed0181d96b9f62b4bae6c488ef' => 'GNU FDL v1.1 (tex)',
    'e48f8585112d62515d7812d748a6f656' => 'GNU FDL v1.1 (texi)',

    'a7281529548874c52f3d2b6aa8b512b3' => 'OpenSSL',

    '0d6be33865b76025c20b48bcac87adb7' => 'automake-1.4 INSTALL',
    '9f3e20fdff9c78aa8e3f9b42be166769' => 'automake-1.7 INSTALL',
);

sub diff_common_licenses
{
    my $file = shift;
    open M, $file or die "$file: $!";
    my $md5sum = Digest::MD5->new->addfile(\*M)->hexdigest();
    close M;
    if ($known_licenses{$md5sum}) {
	print "$file: $known_licenses{$md5sum} variant\n";
	return 1;
    }

    my $diff = 10000;
    my $license = "";
    for my $l (@common_licenses) {
	my $d = `diff -w $l "$file" | wc -l`;
	chomp $d;
	if ($d < $diff) {
	    $license = $l;
	    $diff = $d;
	}
    }
    if ($diff < 125 ) { # licenses/openssl has 127 lines
	print "$file: might be a $license variant ($diff lines diff, $md5sum)\n";
	print "diff -w $license $file\n";
	system "diff -w $license \"$file\"";
	return 1;
    }
    return 0;
}

## get rid of as much work as possible here

sub do_file
{
    my $file = shift || $File::Find::name;
    return if -d $file;
    return if $file =~ /\.svn-base$/;
    return if $file =~ /\/\.svn\//;
    return if $file =~ /\/\.git\//;

    my $type = `file "$file"`;
    if ($type =~ /\bdata\b/) {
	print "$file: binary\n";
	return;
    }

    if ($file =~ /\.(ac|in|m4)$|(^|\/)(configure|ltmain.sh|config\.(sub|guess|rpath)|compile|depcomp|install.sh|missing|mkinstalldirs)$/i) {
	print "$file: auto* cruft\n";
	return;
    }

    if ($file =~ /\.ui$/i) {
	print "$file: KDE interface definition\n";
	return;
    }

    if ($file =~ /(^|\/)(copy|install|li[cz]en|artistic|l?gpl|g?fdl)[^\/]*$/i) {
	diff_common_licenses($file) and return 0;
    }

    grep_file($file);
}

## main

foreach my $arg (@ARGV) {
    if (-d $arg) {
	find({ wanted => \&do_file, no_chdir => 1}, $arg);
    } else {
	do_file($arg);
    }
}

# vim:sw=4:sta:
